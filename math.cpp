#include <iostream>
#include "math.h"

//Return the factorial of n
int factorial(int n){
	if (n==0)
		return 1;
	else 
		return n*factorial(n-1);
}

//Find the greatest common divisor of x and y
int gcd(int x, int y){
	if (x%y==0)
		return y;
	else 
		return gcd(y, x%y);
}

//Calculate the nth fibonacci number recursively
int fib(int n){
	if (n<=0)
		return 0;
	else if (n==1)
		return 1;
	else
		return fib(n-1) + fib(n-2);
}

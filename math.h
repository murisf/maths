#ifndef INC_MATH_H
#define INC_MATH_H

int factorial(int);
int gcd(int, int);
int fib(int);
#endif
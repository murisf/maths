//Needs a better time keeping system implemented

#include <iostream>
#include <time.h>
#include <Windows.h>
//#include <sys/time.h>
using namespace std;

bool RecursiveIsPrime(int, int);
bool nRecursiveIsPrime(int);

int main ()
{
	//For windows
	SYSTEMTIME st;
	int num, n = 2;
	WORD start, end;

	cout << "Enter number: ";
	do
	{
		cin >> num;
	} while (num < 1); 

	//start = gethrtime;
	//start = (int)clock();
	GetSystemTime(&st);
	start = st.wMilliseconds;
	nRecursiveIsPrime(num);
	//end = gethrtime;
	//end = (int)clock();
	GetSystemTime(&st);
	end = st.wMilliseconds;
	cout << "The non-recursive function took " << start - end << " time." << endl;
	
	//start = gethrtime;
	//start = (int)clock();
	GetSystemTime(&st);
	start = st.wMilliseconds;
	RecursiveIsPrime(num,n);
	//end = gethrtime;
	//end = (int)clock();
	GetSystemTime(&st);
	start = st.wMilliseconds;
	cout << "The recursive function took " << start - end << " time." << endl;
	
	system("pause");
	return 0;

}

bool RecursiveIsPrime(int num, int n)
{
	if (num == n) 
		return true;
	if (num % n == 0)
		return false;
	
	return RecursiveIsPrime(num, n+1);
}

bool nRecursiveIsPrime (int num)
{
	if (num == 1)
		return false;
	if (num == 2)
		return true;
	for (int i = 3; i < num; i++)
	{
		if (num % i == 0)
			return false;
	}
	
	return true;
}
